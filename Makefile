CFLAGS = -I paho.mqtt.c/src/ -L paho.mqtt.c/build/output -g
LDLIBS = -l paho-mqtt3c -l ncurses

MQTTLIB = paho.mqtt.c/build/output/libpaho-mqtt3c.so

.PHONY: all clean

all: main

clean:
	rm main

$(MQTTLIB):
	cd paho.mqtt.c && make

main: main.c | $(MQTTLIB)
	$(CC) -o $@ $^ $(CFLAGS) $(LDLIBS)
