#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

#include <ncurses.h>
#include <MQTTClient.h>

#define MQTT_CLIENTID_PRE "testsetup-dashboard-"
#define MQTT_QOS 1

#define LOG(...) do { \
    if (debug) fprintf(logfile, __VA_ARGS__); fflush(logfile); } while (0)
#define LOGDIE(...) do { \
    LOG(__VA_ARGS__); if (debug) fclose(logfile); exit(1); } while (0)
#define ARRSIZE(x) ((sizeof(x))/(sizeof((x)[0])))

struct device {
	const char *name;
	char *statusmsg;
	int statuscode;
};

enum {
	STROBE,
	STEPPER,
	STREAMER,
	WATCHDOG
};

enum {
	CODE_FAIL,
	CODE_OK
};

static MQTTClient client;
static sig_atomic_t should_update = 0;
static int debug = 1;
static FILE *logfile = NULL;
static struct device devices[] = {
	{ "strobe" },
	{ "stepper" },
	{ "streamer" },
	{ "watchdog" },
};

const char *alphabet = " !\"#$%&'()*+,-./0123456789:;<=>?@"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

void
updatesig(int sig)
{
	should_update = 1;
}

void
die(const char *errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	vfprintf(stderr, errstr, ap);
	va_end(ap);

	if (debug && logfile) fclose(logfile);

	exit(1);
}

char*
alloc_fmtstr(const char* fmtstr, ...)
{
	va_list ap;
	size_t size;
	char* str;

	va_start(ap, fmtstr);
	size = vsnprintf(NULL, 0, fmtstr, ap);
	va_end(ap);
	if (size < 0) die("alloc_fmtstr: invalid format string\n");

	size += 1;
	if (!(str = malloc(size))) die("alloc_fmtstr: OOM\n");

	va_start(ap, fmtstr);
	vsnprintf(str, size, fmtstr, ap);
	va_end(ap);

	return str;
}

char*
jsonfind(char *json, const char *key)
{
	char *search, *start, *after, *res;

	search = alloc_fmtstr("\"%s\":\"", key);
	if (!(start = strstr(json, search))) goto fail;

	start += strlen(search);
	if (!(after = strchr(start, '"'))) goto fail;
	res = strndup(start, after - start);
	free(search);
	return res;

fail:
	free(search);
	return NULL;
}

const char*
timestr(time_t t)
{
	static char buf[32];
	strftime(buf, sizeof(buf), "%T", localtime(&t));
	return buf;
}

void
mqtt_send(char *topic, char *payload)
{
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;

	LOG("[MQTT] Publishing to '%s'..\n", topic);

	pubmsg.payload = payload;
	pubmsg.payloadlen = strlen(payload);
	pubmsg.qos = MQTT_QOS;
	pubmsg.retained = 0;
	if (MQTTClient_publishMessage(client, topic, &pubmsg, &token)
			!= MQTTCLIENT_SUCCESS)
		LOG("[MQTT] Error sending message to server\n");
}

void
mqtt_connection_lost(void *ctx, char *cause)
{
	LOGDIE("[MQTT] Connection lost..\n");
}

int
mqtt_receive(void *ctx, char *name, int len, MQTTClient_message *msg)
{
	char *topic, *code;
	int i;

	if (debug)
		LOG("[MQTT] Message received (%s): '%s'\n", name, (char*) msg->payload);

	for (i = 0; i < ARRSIZE(devices); i++) {
		topic = alloc_fmtstr("testsetup/%s/status", devices[i].name);
		if (!strcmp(name, topic)) {
			free(devices[i].statusmsg);
			if (!(devices[i].statusmsg = jsonfind(msg->payload, "msg")))
				devices[i].statusmsg = strdup("INVALID");

			code = jsonfind(msg->payload, "status");
			if (!code) devices[i].statuscode = 1;
			else devices[i].statuscode = !strcmp(code, "ok") ? 0 : 1;
			free(code);

			free(topic);
			break;
		}
		free(topic);
	}

	return 1;
}

void
mqtt_receive_ack(void *ctx, MQTTClient_deliveryToken dt)
{
	LOG("[MQTT] PUBACK received\n");
}

void
mqtt_init(const char *address)
{
	MQTTClient_connectOptions options =
		MQTTClient_connectOptions_initializer;
	int i, rv;
	char *id, *topic;

	id = alloc_fmtstr(MQTT_CLIENTID_PRE "%d", getpid());
	if (MQTTClient_create(&client, address, id,
			MQTTCLIENT_PERSISTENCE_NONE, NULL) != MQTTCLIENT_SUCCESS)
		LOGDIE("[MQTT] Failed to init client\n");
	free(id);

	options.keepAliveInterval = 20;
	options.cleansession = 1;

	MQTTClient_setCallbacks(client, NULL, mqtt_connection_lost,
			mqtt_receive, mqtt_receive_ack);

	if ((rv = MQTTClient_connect(client, &options)) != MQTTCLIENT_SUCCESS)
		LOGDIE("[MQTT] Failed to connect to %s.. (%d)\n", address, rv);

	for (i = 0; i < ARRSIZE(devices); i++) {
		topic = alloc_fmtstr("%s/status", devices[i].name);
		if (MQTTClient_subscribe(client, topic, MQTT_QOS)
				!= MQTTCLIENT_SUCCESS)
			LOGDIE("[MQTT] Failed to subscribe to topic %s\n", topic);

		LOG("[MQTT] Subscribed to topic '%s'\n", topic);
		free(topic);
	}
}

void
update()
{
	char inputbuf[256], *sep, *last, *parts[3], *topic;
	static char *cmdstatus = NULL;
	int i, len, c;

	should_update = 0;

	clear();

	addstr("Device status:\n\n");
	for (i = 0; i < ARRSIZE(devices); i++) {
		printw("%-10s: [%s] %s\n", devices[i].name, devices[i].statuscode ? "OK" : "FAIL",
				devices[i].statusmsg);
	}

	addstr("\n\n\n");
	if (cmdstatus) {
		printw("\n: %s\n", cmdstatus);
		move(getcury(stdscr)-2, 0);
	}
	refresh();

	while (!should_update) {
		printw("[%s] > ", timestr(time(NULL)));
		len = 0;
		while ((c = getch()) != '\n') {
			if (c == EOF) {
				if (should_update) goto next_update;
				continue;
			}
			if (c == KEY_BACKSPACE || c == KEY_DC) {
				if (len) {
					len--;
					mvaddch(getcury(stdscr), getcurx(stdscr)-1, ' ');
					move(getcury(stdscr), getcurx(stdscr)-1);
					refresh();
				}
				continue;
			}
			if (!strchr(alphabet, c)) continue;
			inputbuf[len] = c;
			addch(c);
			len++;
		}
		inputbuf[len] = '\0';

		move(getcury(stdscr), 0);
		for (i = 0; i < getmaxx(stdscr); i++) addch(' ');

		last = inputbuf;
		for (i = 0; i < ARRSIZE(parts); i++) {
			sep = strchr(last, ' ');
			if (!sep) {
				if (i == ARRSIZE(parts) - 1) {
					sep = last + strlen(last);
				} else {
					cmdstatus = "INVALID";
					break;
				}
			}
			parts[i] = last;
			*sep = '\0';
			last = sep + 1;
		}

		if (i == ARRSIZE(parts)) {
			topic = alloc_fmtstr("testsetup/%s/%s",
					parts[0], parts[1]);
			mqtt_send(topic, parts[2]);
			free(topic);
			cmdstatus = "DONE";
		}

		printw("%s: %s\n", cmdstatus, inputbuf);

		move(getcury(stdscr)-2, 0);
	}

next_update:
	return;
}

int
main(int argc, const char **argv)
{
	if (argc < 2)
		die("Usage: dashboard <testsetup-ip>\n");

	signal(SIGALRM, updatesig);

	if (debug) logfile = fopen("log", "w+");

	mqtt_init(argv[1]);

	initscr();
	halfdelay(1);      /* use 10th second timeout on getch */
	keypad(stdscr, 1); /* interpret escape sequences */
	noecho();          /* dont output what we type, output whats interpreted */
	while (1) update();
	endwin();

	MQTTClient_disconnect(client, 0);
	MQTTClient_destroy(&client);
}
