#!/bin/bash

SCRIPTPATH="$(dirname $(readlink -f "$0"))"
cd "$SCRIPTPATH"

prefix=""
[ ! -z "$TEST" ] && prefix="valgrind --leak-check=full"

LD_LIBRARY_PATH="paho.mqtt.c/build/output" $prefix ./main $@
